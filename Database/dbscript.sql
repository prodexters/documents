-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.6.10 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2015-08-17 21:55:39
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for dexter
DROP DATABASE IF EXISTS `dexter`;
CREATE DATABASE IF NOT EXISTS `dexter` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dexter`;


-- Dumping structure for table dexter.area_master
DROP TABLE IF EXISTS `area_master`;
CREATE TABLE IF NOT EXISTS `area_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `area` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table dexter.brand
DROP TABLE IF EXISTS `brand`;
CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` varchar(256) NOT NULL,
  `logo` varchar(256) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.category_master
DROP TABLE IF EXISTS `category_master`;
CREATE TABLE IF NOT EXISTS `category_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.offer
DROP TABLE IF EXISTS `offer`;
CREATE TABLE IF NOT EXISTS `offer` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `discount` varchar(50) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `offer_code` varchar(50) NOT NULL,
  `expire_on` bigint(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_offer_seller` (`seller_id`),
  KEY `FK_offer_brand` (`brand_id`),
  KEY `FK_offer_category_master` (`category_id`),
  CONSTRAINT `FK_offer_brand` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
  CONSTRAINT `FK_offer_category_master` FOREIGN KEY (`category_id`) REFERENCES `category_master` (`id`),
  CONSTRAINT `FK_offer_seller` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.offers_tags_mapping
DROP TABLE IF EXISTS `offers_tags_mapping`;
CREATE TABLE IF NOT EXISTS `offers_tags_mapping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `offer_id` int(10) NOT NULL DEFAULT '0',
  `tag_id` int(10) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_offers_tags_mapping_offer` (`offer_id`),
  KEY `FK_offers_tags_mapping_tag_master` (`tag_id`),
  CONSTRAINT `FK_offers_tags_mapping_offer` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`),
  CONSTRAINT `FK_offers_tags_mapping_tag_master` FOREIGN KEY (`tag_id`) REFERENCES `tag_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.offer_store_mapping
DROP TABLE IF EXISTS `offer_store_mapping`;
CREATE TABLE IF NOT EXISTS `offer_store_mapping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `offer_id` int(10) NOT NULL,
  `store_id` int(10) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_offer_store_mapping_offer` (`offer_id`),
  KEY `FK_offer_store_mapping_store` (`store_id`),
  CONSTRAINT `FK_offer_store_mapping_offer` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`),
  CONSTRAINT `FK_offer_store_mapping_store` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.seller
DROP TABLE IF EXISTS `seller`;
CREATE TABLE IF NOT EXISTS `seller` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.seller_store_mapping
DROP TABLE IF EXISTS `seller_store_mapping`;
CREATE TABLE IF NOT EXISTS `seller_store_mapping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) NOT NULL,
  `store_id` int(10) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_seller_store_mapping_seller` (`seller_id`),
  KEY `FK_seller_store_mapping_store` (`store_id`),
  CONSTRAINT `FK_seller_store_mapping_seller` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`),
  CONSTRAINT `FK_seller_store_mapping_store` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.store
DROP TABLE IF EXISTS `store`;
CREATE TABLE IF NOT EXISTS `store` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `address_line_1` varchar(256) NOT NULL,
  `address_line_2` varchar(256) NOT NULL,
  `area_id` int(10) NOT NULL,
  `city` varchar(128) NOT NULL,
  `country` varchar(128) NOT NULL,
  `latitude` bigint(20) NOT NULL,
  `longitude` bigint(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_store_area_master` (`area_id`),
  CONSTRAINT `FK_store_area_master` FOREIGN KEY (`area_id`) REFERENCES `area_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table dexter.store_brand_mapping
DROP TABLE IF EXISTS `store_brand_mapping`;
CREATE TABLE IF NOT EXISTS `store_brand_mapping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `store_id` int(10) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_store_brand_mapping_store` (`store_id`),
  KEY `FK_store_brand_mapping_brand` (`brand_id`),
  CONSTRAINT `FK_store_brand_mapping_brand` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`),
  CONSTRAINT `FK_store_brand_mapping_store` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.sub_category_master
DROP TABLE IF EXISTS `sub_category_master`;
CREATE TABLE IF NOT EXISTS `sub_category_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `subcategory` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sub_category_master1_category_master` (`category_id`),
  CONSTRAINT `FK_sub_category_master1_category_master` FOREIGN KEY (`category_id`) REFERENCES `category_master` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.tag_master
DROP TABLE IF EXISTS `tag_master`;
CREATE TABLE IF NOT EXISTS `tag_master` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email_id` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_id` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.


-- Dumping structure for table dexter.user_contact_details
DROP TABLE IF EXISTS `user_contact_details`;
CREATE TABLE IF NOT EXISTS `user_contact_details` (
  `user_id` int(10) NOT NULL,
  `mobile_no` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` bigint(20) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_on` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `FK_user_contact_details_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Data exporting was unselected.
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
